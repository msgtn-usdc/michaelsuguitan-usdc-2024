/** 
 * RECOMMENDATION
 * 
 * To test your code, you should open "tester.html" in a web browser.
 * You can then use the "Developer Tools" to see the JavaScript console.
 * There, you will see the results unit test execution. You are welcome
 * to run the code any way you like, but this is similar to how we will
 * run your code submission.
 * 
 * The Developer Tools in Chrome are available under the "..." menu, 
 * futher hidden under the option "More Tools." In Firefox, they are 
 * under the hamburger (three horizontal lines), also hidden under "More Tools." 
 */


/**
 * Searches for matches in scanned text.
 * @param {string} searchTerm - The word or term we're searching for. 
 * @param {JSON} scannedTextObj - A JSON object representing the scanned text.
 * @returns {JSON} - Search results.
 * */ 
 function findSearchTermInBooks(searchTerm, scannedTextObj) {
    /** You will need to implement your search and 
     * return the appropriate object here. */

    // NOTE - capitalization counts - "The" should not match "the"
    var results = [];
    // Iterate through books
    for (bookObj of scannedTextObj) {
        // Iterate through lines in a book
        let numberOfLines = bookObj.Content.length;
        for (const [lineIndex,content] of bookObj.Content.entries()) {
            let currentLine = content.Text;

            // Handle hyphen-split words
            // If the line ends with a dash, erase the dash and append the remnant of the split word from the next line
            let endsInDash = currentLine.charAt(currentLine.length-1) == '-';
            // Check if this is the last line
            let notLastLine = lineIndex<(numberOfLines-1);
            // Store the remnant of the split word from the next line
            let splitWordRem = '';
            if (endsInDash && notLastLine) {
                let nextText = bookObj.Content[lineIndex+1].Text;
                splitWordRem = nextText.split(' ')[0];
                currentLine = currentLine.substring(0,currentLine.length-1) + splitWordRem;
            }

            // Check if the current line includes the search term
            // If the search term is the remnant of the split word, don't include this page in the results
            if (currentLine.includes(searchTerm) && splitWordRem != searchTerm) {
                results.push({
                    "ISBN": bookObj.ISBN,
                    "Page": content.Page,
                    "Line": content.Line,
                });
            }
        }
    }

    var result = {
        "SearchTerm": searchTerm,
        "Results": results
    };
    
    return result; 
}

/** Example input object. */
const twentyLeaguesIn = [
    {
        "Title": "Twenty Thousand Leagues Under the Sea",
        "ISBN": "9780000528531",
        "Content": [
            {
                "Page": 31,
                "Line": 8,
                "Text": "now simply went on by her own momentum.  The dark-"
            },
            {
                "Page": 31,
                "Line": 9,
                "Text": "ness was then profound; and however good the Canadian\'s"
            },
            {
                "Page": 31,
                "Line": 10,
                "Text": "eyes were, I asked myself how he had managed to see, and"
            } 
        ] 
    }
]
    
/** Example output object */
const twentyLeaguesOut = {
    "SearchTerm": "the",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 9
        }
    ]
}

/*
 _   _ _   _ ___ _____   _____ _____ ____ _____ ____  
| | | | \ | |_ _|_   _| |_   _| ____/ ___|_   _/ ___| 
| | | |  \| || |  | |     | | |  _| \___ \ | | \___ \ 
| |_| | |\  || |  | |     | | | |___ ___) || |  ___) |
 \___/|_| \_|___| |_|     |_| |_____|____/ |_| |____/ 
                                                      
 */

/* We have provided two unit tests. They're really just `if` statements that 
 * output to the console. We've provided two tests as examples, and 
 * they should pass with a correct implementation of `findSearchTermInBooks`. 
 * 
 * Please add your unit tests below.
 * */

/** Initialize the test number counter */
var testNumber = 1;
function printPassFail(testPassed, testActual, testOutput) {
    if (testPassed) {
        console.log(`PASS: Test ${testNumber}`);
    } else {
        console.log(`FAIL: Test ${testNumber}`);
        console.log("Expected:", testActual);
        console.log("Received:", testOutput);
    }
    // Increment the test number counter
    testNumber++;
}

function checkResult(testActual, testOutput) {
    let testPassed = JSON.stringify(testActual) === JSON.stringify(testOutput);
    printPassFail(testPassed, testActual, testOutput);
}

function checkResultLength(actualLength, outputLength) {
    let testPassed = actualLength == outputLength;
    printPassFail(testPassed, actualLength, outputLength);
}

/** We can check that, given a known input, we get a known output. */
var testResult = findSearchTermInBooks("the", twentyLeaguesIn);
checkResult(twentyLeaguesOut, testResult);

/** We could choose to check that we get the right number of results. */
testResult = findSearchTermInBooks("the", twentyLeaguesIn); 
checkResultLength(1, testResult.Results.length)

const capitalTheOut = {
    "SearchTerm": "The",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 8
        }
    ]
};
/** Capital "The" should only match on line 8 */
testResult = findSearchTermInBooks("The", twentyLeaguesIn);
checkResult(capitalTheOut, testResult);

const repeatedThe = [
    {
        "Title": "The",
        "ISBN": "9780000528531",
        "Content": [
            {
                "Page": 31,
                "Line": 8,
                "Text": "the the the the"
            },
        ] 
    }
]
/** Multiple instances on the same line should only match once */
testResult = findSearchTermInBooks("the", repeatedThe);
checkResultLength(1,testResult.Results.length);

/** Case-sensitive test
Searching for "The" should not match "the" */
testResult = findSearchTermInBooks("The", repeatedThe);
checkResultLength(0, testResult.Results.length);

/** Negative tests with an empty list of books */
const emptyInput = []
testResult = findSearchTermInBooks("The", emptyInput);
checkResultLength(0,testResult.Results.length);

const bookWithNoInput = [
    {
        "Title": "Book With No Input",
        "ISBN": "9780000528531",
        "Content": [] 
    }
]
/** Negative test with a book that has no text lines */
testResult = findSearchTermInBooks("The", bookWithNoInput)
checkResultLength(0,testResult.Results.length);

const darknessLeaguesOut = {
    "SearchTerm": "darkness",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 8
        }
    ]
};
/** If a word is broken by a hyphen, return the line on which it starts */
testResult = findSearchTermInBooks("darkness", twentyLeaguesIn);
checkResult(darknessLeaguesOut, testResult);

const nessOut  = {
    "SearchTerm": "ness",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 9
        }
    ]
};
/** Match partial words */
testResult = findSearchTermInBooks("ness", twentyLeaguesIn);
checkResult(nessOut, testResult);

const canadiansOut = {
    "SearchTerm": "Canadian's",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 9
        }
    ]
};
/** We should still match unescaped punctuation e.g. apostrophe */
testResult = findSearchTermInBooks("Canadian's", twentyLeaguesIn);
checkResult(canadiansOut, testResult);